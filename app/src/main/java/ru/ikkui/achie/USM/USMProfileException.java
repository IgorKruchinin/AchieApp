package ru.ikkui.achie.USM;

public class USMProfileException extends Exception {
    USMProfileException(String message) {
        super(message);
    }
}
