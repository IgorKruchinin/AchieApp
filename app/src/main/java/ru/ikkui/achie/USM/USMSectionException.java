package ru.ikkui.achie.USM;

public class USMSectionException extends Exception {
    public USMSectionException(String message) {
        super(message);
    }
}
